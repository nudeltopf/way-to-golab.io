Puoi giocare quasi ovunque sul goban, ma ci sono posti dove
le regole impediscono di posizionare una pietra.

Bianco ha la possibilità di giocare nel triangolo.

Se nero ci giocasse, la sua pietra sarebbe già circondata, perciò
a nero non è concesso giocarci.

Board::illegal

---

In questo caso, tuttavia, a nero è permesso giocare nel triangolo
perchè così facendo può catturare le due pietre bianche.

Board::legal

---

Dopo che nero ha posizionato la pietra, questa sarà la posizione
sul goban.

Board::captured
