After you read this page, you'll know all the rules!

The concept of **ko** is a bit more difficult than the rules we've looked at so
far.  As a bit of relevant context, **ko** is a Japanese word for "eternity".
Let's look at why that is.

One white stone is in atari.  Capture it!

Board::simple-ko

Notice that in the position above, it would be white's turn and black is now in
atari.  If black were to capture white, we would be back to where we started!

It's possible that this goes on forever!  To guard against this, **the ko
rule prohibits repeating board positions**.  You can try to play to capture the
black stone above, but it won't be allowed.

Can white ever re-capture the black stone?

The answer is yes, but must play elsewhere first.  This way, a different board
position is produced.

**You can take the ko stone back once you play somewhere else.**

Try playing elsewhere above, then recapturing the ko stone.

---

So how does this manifest in actual games?

You can play both sides here where it is begins with black to move.  Black can
capture the ko where both sides have 5 stones at stake.

White can followup by threatening the capture of the lower group of 8 stones.

Board::ko-fight

When a player plays away from a ko to make a threatening move, it is called a
**ko threat**.  Both players continue to trade threats until one player
decides the threat is not big enough to warrant a response.

This situation is called a **ko fight**.

---

Ko fights can produce incredibly complex situations across the board.  For a
beginner, you really just need to remember the rule.

**Moves that produce the same board positions are prohibited.**
