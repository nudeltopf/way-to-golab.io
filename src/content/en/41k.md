Sometimes you can sacrifice a stone to capture your opponent's group.  This
technique is called a **snapback**.

Try to capture 4 stones while looking for a sacrificial move.

Board::snapback-1

---

This is similar to the previous problem.  Notice that if black succeeds, all
the black stones will effectively be connected.

Board::snapback-2

---

**Snapbacks** can also occur on the edge.

Board::snapback-3
