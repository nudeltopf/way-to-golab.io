Andremo a dare uno sguardo al finale di una partita e a come il punteggio
viene calcolato.
Useremo un goban 5x5 così che sia più semplice da capire.

## Territorio

Board::end-game-1

Entrambi i giocatori hanno fatto 5 mosse ciascuno e la partita è considerata
finita.

Nero ha 5 punti sulla sinistra e bianco ha 10 punti sulla destra, quindi
bianco vince di 5 punti.

Gli spazi aperti, inclusi gli angoli e i bordi, vengono contati. Le pietre, invece,
non vengono contate.

---

Una partita è considerata finita quando entrambi i giocatori decidono che non
vogliono più aggiungere altre mosse.

Perchè la partita nella prima figura è considerata conclusa?

Board::end-game-2

Se continuiamo la partita, possiamo vedere che se nero gioca nel territorio bianco,
quest'ultimo può catturare facilmente la pietra nera.

---

D'altra parte, se nero decide di giocare all'interno del suo territorio, i suoi punti
punti diminuiscono.

Board::end-game-3

Detto questo, considera che a volte ha senso giocare nel proprio territorio se si sta
cercando di formare due occhi per vivere o per rinforzare un muro in modo da prevenire
un'invasione.

---

Anche questo è il finale di una partita. Possiamo contare 5 punti per nero e
6 punti per bianco.

Board::end-game-4

E riguardo ai due punti al centro? Questi punti sono chiamati **dame** -
punti neutrali che non vengono riconosciuti a nessuno dei due giocatori.

Uno qualunque dei due giocatori può giocarci, ma nessun punto verrebbe
guadagnato o perso.

## Quando finisce una partita?

* Quando un giocatore decide che non vuole più giocare da nessuna parte, egli
  può passare. Se entrambi i giocatori passano uno dopo l'altro, la partita
  finisce. I territori vengono contati, tenendo anche conto delle pietre catturate,
  andando a formare il punteggio totale di ogni giocatore. Quello con il maggior
  numero di punti vince.
* Quando un giocatore pensa che non è più possibile vincere a prescindere da quale mossa
  giochi, egli può arrendersi.

Al contrario di molti giochi, la resa è considerata un risultato onorevole. Continuare
a giocare di fronte ad una sconfitta schiacciante invece non lo è.
